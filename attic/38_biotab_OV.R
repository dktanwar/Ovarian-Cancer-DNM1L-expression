###################################################
## Title:   OV Data DNM1L 76 samples             ##
## Author:  Deepak Tanwar, Malay Basu            ##
## Date:    March 4, 2015                        ##
###################################################

### This script will claculate the LFC between DNM1L and all other genes (average and among all samples individually)

biotab_recc <- read.table("../TCGA_Biotab/OV//nationwidechildrens.org_clinical_follow_up_v1.0_nte_ov.txt", sep = "\t", header = T)[-c(1:2),] 

OV_can_log2 <- read.table("../results/14_OV_Data_extraction/log2_OV_exp_data.txt", sep = "\t", check.names = F)

length(intersect(biotab_recc$bcr_patient_barcode, substr(colnames(OV_can_log2), 1,12)))

clinical_patient <- read.table("../TCGA_Biotab/OV/nationwidechildrens.org_clinical_patient_ov.txt", sep = "\t", header = T, 
                               check.names = F)[-c(1:2),]

OV_sdrf <- read.table("../data/sdrf/unc.edu_OV.IlluminaHiSeq_RNASeqV2.1.1.0.sdrf.txt.bz2", sep = "\t", header = T, check.names = F)

intersect(clinical_patient$bcr_patient_uuid, OV_sdrf$Extract.Name)

