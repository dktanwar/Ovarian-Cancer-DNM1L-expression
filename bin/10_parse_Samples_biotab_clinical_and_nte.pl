#!/usr/bin/perl -w

use strict;

my $file = shift;

print join("\t", "#Patient_ID", "RNASeq_Sample_ID", "primary_therapy_outcome_success", "new_neoplasm_event_type"), "\n";

open (my $fh, $file) || die "can't open the file: $file";

my %hash;

while (my $line = <$fh>){
    chomp $line;
    my @split = split(/\t/, $line);
    next if $split[0] eq "patients";
    my $key = $split[0] . "\t" . $split[1];
    if (exists $hash{$key}){
	my @k = split(/\t/, $hash{$key});
	if($split[2] eq $k[0] & $split[3] eq $k[1]){
	    next;
	}elsif ($split[2] ne $k[0] & $split[3] eq $k[1]){
	    my $s = join("|", $k[0], $split[2]);
	    $hash{$key} = join("\t", $s, $split[3]);
	}elsif ($split[2] eq $k[0] & $split[3] ne $k[1]){
	    my $s = join("|", $k[1], $split[3]);
	    $hash{$key} = join("\t", $split[2], $s);
	}
	else{
	    my $s1 = join("|", $k[0], $split[2]);
	    my $s2 = join("|", $k[1], $split[3]);
	    $hash{$key} = join("\t", $s1, $s2);
	}
    }else{
    $hash{$key} = join("\t", $split[2], $split[3]);
    }
}

foreach my $print (keys %hash){
    print $print, "\t", $hash{$print}, "\n";

}
